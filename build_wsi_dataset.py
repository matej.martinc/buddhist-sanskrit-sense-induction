import pandas as pd
from sklearn.model_selection import KFold
from sklearn.model_selection import StratifiedKFold
import numpy as np
import random


def generate_datasets(input_path, tokenizer, class_distrib):
    print('Generating datasets')
    df = pd.read_csv(input_path, sep=',', encoding='utf8')
    all_lemmas = np.array(sorted(list(set(df.lemma.tolist()))))
    kf = KFold(n_splits=5, shuffle=True, random_state=2023)
    all_datasets = []
    for train_index, test_index in kf.split(all_lemmas):
        tt_data = []
        train_lemmas = all_lemmas[train_index]
        test_lemmas = all_lemmas[test_index]
        print("Train lemmas", train_lemmas)
        print("Test lemmas", test_lemmas)
        id = 0
        for idx, lemmas in enumerate([train_lemmas, test_lemmas]):
            examples = []
            for lemma in lemmas:
                lemma_examples_pos = []
                lemma_examples_neg = []
                count_0 = 0
                count_1 = 0
                df_lemma = df[df['lemma']==lemma]
                for idx_1, row_1 in df_lemma.iterrows():
                    for idx_2, row_2 in df_lemma.iterrows():
                        id += 1
                        if idx_1 != idx_2:
                            sent_1 = row_1['kwic']
                            sent_1_id = int(row_1['sID'])
                            sent_1 = sent_1.replace('-', ' ').replace('^', ' ').replace("'", ' ').replace(".", ' ')
                            sent_1 = sent_1.replace('<b>', ' [TWS] ').replace('</b>', ' [TWE] ')

                            sent_2 = row_2['kwic']
                            sent_2_id = int(row_2['sID'])
                            sent_2 = sent_2.replace('-', ' ').replace('^', ' ').replace("'", ' ').replace(".", ' ')
                            sent_2 = sent_2.replace('<b>', ' [TWS] ').replace('</b>', ' [TWE] ')

                            if row_1['sense'] == row_2['sense']:
                                label = 0
                                count_0 += 1
                                preprocessed_example = '[CLS] ' + sent_1 + ' [SEP] [CLS] ' + sent_2 + ' [SEP]'
                                if preprocessed_example.count(' [TWS] ') == 2 and len(tokenizer.tokenize(preprocessed_example)) <= 128:
                                    lemma_examples_pos.append([id, lemma, sent_1, sent_1_id, sent_2, sent_2_id,
                                                               preprocessed_example, label,
                                                               row_1['sense'], row_2['sense']])
                            else:
                                label = 1
                                count_1 += 1
                                preprocessed_example = '[CLS] ' + sent_1 + ' [SEP] [CLS] ' + sent_2 + ' [SEP]'
                                if preprocessed_example.count(' [TWS] ') == 2 and len(tokenizer.tokenize(preprocessed_example)) <= 128:
                                    lemma_examples_neg.append([id, lemma, sent_1, sent_1_id, sent_2, sent_2_id,
                                                           preprocessed_example, label,
                                                           row_1['sense'], row_2['sense']])
                if class_distrib == 'balanced':
                    if idx == 0:
                        if len(lemma_examples_pos) > len(lemma_examples_neg) and len(lemma_examples_neg) > 0:
                            lemma_examples_pos = random.sample(lemma_examples_pos, len(lemma_examples_neg))
                        elif len(lemma_examples_neg) > len(lemma_examples_pos) and len(lemma_examples_pos) > 0:
                            lemma_examples_neg = random.sample(lemma_examples_neg, len(lemma_examples_pos))
                        if len(lemma_examples_neg) > 0 and len(lemma_examples_pos) > 0:
                            examples.extend(lemma_examples_pos)
                            examples.extend(lemma_examples_neg)
                    else:
                        examples.extend(lemma_examples_pos)
                        examples.extend(lemma_examples_neg)
                else:
                    examples.extend(lemma_examples_pos)
                    examples.extend(lemma_examples_neg)

            df_wsi = pd.DataFrame(examples, columns=['id', 'lemma', 'sent_1', 'sent_1_id', 'sent_2', 'sent_2_id', 'preprocessed_example', 'target', 'sense_1', 'sense_2'])
            tt_data.append(df_wsi)
        all_datasets.append(tt_data)
    print('Generation done')
    return all_datasets


def generate_mixed_datasets(input_path, tokenizer, class_distrib):
    print('Generating datasets')
    df = pd.read_csv(input_path, sep=',', encoding='utf8')
    print("Original dataset len", len(df))
    df = df.sample(frac=1, random_state=2023).reset_index(drop=True)
    all_lemmas = sorted(list(set(df.lemma.tolist())))
    n_folds = 5
    all_datasets = [([],[]) for _ in range(n_folds)]
    id = 0
    for lemma in all_lemmas:
        df_lemma_all = df[df['lemma'] == lemma]
        skf = StratifiedKFold(n_splits=n_folds, shuffle=True, random_state=123)
        X = df_lemma_all.drop('sense', axis=1)
        y = df_lemma_all.sense
        fold = 0
        for train_index, test_index in skf.split(X, y):
            df_train_lemma = df_lemma_all.iloc[train_index]
            df_test_lemma = df_lemma_all.iloc[test_index]
            for idx, df_lemma_chunk in enumerate([df_train_lemma, df_test_lemma]):
                lemma_chunk_examples_pos = []
                lemma_chunk_examples_neg = []
                lemma_chunk_examples = []
                for idx_1, row_1 in df_lemma_chunk.iterrows():
                    for idx_2, row_2 in df_lemma_chunk.iterrows():
                        id += 1
                        if idx_1 != idx_2:
                            sent_1 = row_1['kwic']
                            sent_1_id = int(row_1['sID'])
                            sent_1 = sent_1.replace('-', ' ').replace('^', ' ').replace("'", ' ').replace(".", ' ')
                            sent_1 = sent_1.replace('<b>', ' [TWS] ').replace('</b>', ' [TWE] ')
                            sent_2 = row_2['kwic']
                            sent_2_id = int(row_2['sID'])

                            sent_2 = sent_2.replace('-', ' ').replace('^', ' ').replace("'", ' ').replace(".", ' ')
                            sent_2 = sent_2.replace('<b>', ' [TWS] ').replace('</b>', ' [TWE] ')

                            if row_1['sense'] == row_2['sense']:
                                label = 0
                                preprocessed_example = '[CLS] ' + sent_1 + ' [SEP] [CLS] ' + sent_2 + ' [SEP]'
                                if preprocessed_example.count(' [TWS] ') == 2 and len(
                                        tokenizer.tokenize(preprocessed_example)) <= 128:
                                    lemma_chunk_examples_pos.append([id, lemma, sent_1, sent_1_id, sent_2, sent_2_id,
                                                               preprocessed_example, label,
                                                               row_1['sense'], row_2['sense']])
                            else:
                                label = 1
                                preprocessed_example = '[CLS] ' + sent_1 + ' [SEP] [CLS] ' + sent_2 + ' [SEP]'
                                if preprocessed_example.count(' [TWS] ') == 2 and len(
                                        tokenizer.tokenize(preprocessed_example)) <= 128:
                                    lemma_chunk_examples_neg.append([id, lemma, sent_1, sent_1_id, sent_2, sent_2_id,
                                                                                    preprocessed_example, label,
                                                                                    row_1['sense'], row_2['sense']])
                if class_distrib == 'balanced':
                    if idx == 0:
                        if len(lemma_chunk_examples_pos) > len(lemma_chunk_examples_neg) and len(lemma_chunk_examples_neg) > 0:
                            lemma_chunk_examples_pos = random.sample(lemma_chunk_examples_pos, len(lemma_chunk_examples_neg))
                        elif len(lemma_chunk_examples_neg) > len(lemma_chunk_examples_pos) and len(lemma_chunk_examples_pos) > 0:
                            lemma_chunk_examples_neg = random.sample(lemma_chunk_examples_neg, len(lemma_chunk_examples_pos))
                        if len(lemma_chunk_examples_neg) > 0 and len(lemma_chunk_examples_pos) > 0:
                            lemma_chunk_examples.extend(lemma_chunk_examples_pos)
                            lemma_chunk_examples.extend(lemma_chunk_examples_neg)
                    else:
                        lemma_chunk_examples.extend(lemma_chunk_examples_pos)
                        lemma_chunk_examples.extend(lemma_chunk_examples_neg)
                else:
                    lemma_chunk_examples.extend(lemma_chunk_examples_pos)
                    lemma_chunk_examples.extend(lemma_chunk_examples_neg)
                all_datasets[fold][idx].extend(lemma_chunk_examples)
            fold += 1
    all_df_datasets = []
    for fold_idx, f in enumerate(all_datasets):
        tt_data = []
        for idx, examples in enumerate(f):
            df_wsi = pd.DataFrame(examples, columns=['id', 'lemma', 'sent_1', 'sent_1_id', 'sent_2', 'sent_2_id',
                                                     'preprocessed_example', 'target', 'sense_1', 'sense_2'])
            if idx == 0:
                df_wsi.to_csv('data/wsi_binary_train_' + str(fold_idx + 1) + '.tsv', sep='\t', encoding='utf8', index=False)
            else:
                df_wsi = df_wsi.sort_values(["lemma"], ignore_index=True)
                df_wsi.to_csv('data/wsi_binary_test_' + str(fold_idx + 1) + '.tsv', sep='\t', encoding='utf8', index=False)
            print("Num examples in final dataset", len(examples))
            tt_data.append(df_wsi)
        all_df_datasets.append(tt_data)

    print('Generation done')
    return all_df_datasets

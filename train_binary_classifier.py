from transformers import get_linear_schedule_with_warmup
import torch
import numpy as np
import pandas as pd
from sklearn.metrics import precision_score, recall_score, accuracy_score, f1_score
from torch import nn
from torch.utils.data import Dataset, DataLoader
import os
from collections import Counter
from transformers import AutoModel
from transformers import AutoTokenizer
from build_wsi_dataset import generate_datasets, generate_mixed_datasets
import argparse

os.environ["TOKENIZERS_PARALLELISM"] = "false"


class WSIClassifier(nn.Module):

  def __init__(self, n_classes, model_path):
    super(WSIClassifier, self).__init__()
    self.encoder = AutoModel.from_pretrained(model_path)
    self.drop = nn.Dropout(p=0.3)
    self.out = nn.Linear(self.encoder.config.hidden_size * 2, n_classes)
    self.dense = nn.Linear(self.encoder.config.hidden_size * 2, self.encoder.config.hidden_size * 2)
    self.activation = nn.Tanh()

  def get_indices(self, target_values, logits):
    tensor_list_1 = []
    tensor_list_2 = []
    for idx, batch in enumerate(target_values):
      tensor_list_1.append(logits[idx, batch[0], :])
      tensor_list_2.append(logits[idx, batch[1], :])
    targets = [torch.stack(tensor_list_1), torch.stack(tensor_list_2)]
    return targets

  def forward(self, input_ids, attention_mask, target_tokens):
    outputs = self.encoder(
      input_ids=input_ids,
      attention_mask=attention_mask
    )

    last_hidden_state = self.drop(outputs['last_hidden_state'])
    pooler = self.get_indices(target_tokens, last_hidden_state)
    pooler = torch.cat(pooler, dim=1)
    pooled_output = self.dense(pooler)
    output = self.activation(pooled_output)
    return self.out(output)

class SanskritDataset(Dataset):

  def __init__(self, docs, targets, ids, tokenizer, max_len):
    self.docs = docs
    self.targets = targets
    self.tokenizer = tokenizer
    self.max_len = max_len
    self.ids = ids
    self.tws_token_id = self.tokenizer('[TWS]')['input_ids'][0]

  def __len__(self):
    return len(self.docs)

  def __getitem__(self, item):
    doc = str(self.docs[item])
    target = self.targets[item]
    id = self.ids[item]
    doc = doc.replace('-', ' ').replace('^', ' ').replace("'", ' ').replace(".", ' ').replace('<b>', '').replace('</b>', '')
    encoding = self.tokenizer.encode_plus(
      doc,
      add_special_tokens=False,
      truncation=True,
      max_length=self.max_len,
      return_token_type_ids=False,
      padding='max_length',
      return_attention_mask=True,
      return_tensors='pt',
    )
    input_ids = encoding['input_ids'].flatten()
    target_tokens = np.array([0 if x != self.tws_token_id else 1 for x in input_ids])
    target_tokens = np.roll(target_tokens, 1)
    target_tokens = np.nonzero(target_tokens)[0]

    return {
      'doc': doc,
      'input_ids': input_ids,
      'attention_mask': encoding['attention_mask'].flatten(),
      'targets': torch.tensor(target, dtype=torch.long),
      'ids': id,
      'target_tokens': torch.tensor(target_tokens, dtype=torch.long),
    }


def create_data_loader(df, tokenizer, max_len, batch_size):
  ds = SanskritDataset(
      docs=df.preprocessed_example.to_numpy(),
      targets=df["target"].to_numpy(),
      tokenizer=tokenizer,
      max_len=max_len,
      ids=df['id'].to_numpy()
  )
  return DataLoader(
    ds,
    batch_size=batch_size,
    num_workers=4
  )


def train_epoch(model, data_loader, loss_fn, optimizer, device, scheduler, n_examples):
  model = model.train()
  losses = []
  correct_predictions = 0
  counter = 0

  for d in data_loader:
    counter += 1
    input_ids = d["input_ids"].to(device)
    attention_mask = d["attention_mask"].to(device)
    targets = d["targets"].to(device)
    target_tokens = d["target_tokens"].to(device)
    output = model(
      input_ids=input_ids,
      attention_mask=attention_mask,
      target_tokens=target_tokens
    )

    _, preds = torch.max(output, dim=1)
    loss = loss_fn(output, targets)
    if counter % 100 == 0:
      print(f'Avg loss at step {str(counter)}/{str(len(data_loader))}: {str(sum(losses)/len(losses))}')
    correct_predictions += torch.sum(preds == targets)
    losses.append(loss.item())
    loss.backward()
    nn.utils.clip_grad_norm_(model.parameters(), max_norm=1.0)
    optimizer.step()
    scheduler.step()
    optimizer.zero_grad()

  return correct_predictions.double() / n_examples, np.mean(losses)


def eval_model(model, data_loader, loss_fn, device, n_examples):
  model = model.eval()
  losses = []
  correct_predictions = 0
  all_preds = []
  all_targets = []

  with torch.no_grad():
    for d in data_loader:
      input_ids = d["input_ids"].to(device)
      attention_mask = d["attention_mask"].to(device)
      targets = d["targets"].to(device)
      target_tokens = d["target_tokens"].to(device)

      output = model(
        input_ids=input_ids,
        attention_mask=attention_mask,
        target_tokens=target_tokens
      )

      _, preds = torch.max(output, dim=1)
      loss = loss_fn(output, targets)
      correct_predictions += torch.sum(preds == targets)
      losses.append(loss.item())
      all_preds.extend(preds.tolist())
      all_targets.extend(targets.tolist())

  return correct_predictions.double() / n_examples, np.mean(losses), all_preds, all_targets



if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument("--pretrained_model_name", default="Matej/bert-base-buddhist-sanskrit-v2",
                      help="Transformer model name on huggingface")
  parser.add_argument("--data_path", default='data/wsi_lemmas_clean.csv',
                      help="Path to data")
  parser.add_argument("--output_folder_path", default='results',
                      help="Path to output folder containing trained models and prediction results")
  parser.add_argument("--device",
                      default='cuda',
                      help="gpu device")
  parser.add_argument("--mode",
                      default='no_lemma_division',
                      help="two modes are available: 'lemma_division' or 'no_lemma_division'")
  parser.add_argument("--class_distrib",
                      default='balanced',
                      help="balanced or unbalanced classes in train set")
  parser.add_argument("--num_epochs",
                      default=5,
                      type=int,
                      help="balanced or unbalanced classes in train set")
  parser.add_argument("--max_len",
                      default=128,
                      type=int,
                      help="max input sequence length")
  parser.add_argument("--batch_size",
                      default=64,
                      type=int,
                      help="batch size")
  args = parser.parse_args()

  tokenizer = AutoTokenizer.from_pretrained(args.pretrained_model_name, use_fast=True)
  tokenizer.pad_token = '[PAD]'
  tokenizer.unk_token = '[UNK]'
  tokenizer.cls_token = '[CLS]'
  tokenizer.sep_token = '[SEP]'
  tokenizer.mask_token = '[MASK]'
  tokenizer.add_tokens(['[TWS]', '[TWE]'])
  if args.mode == 'lemma_division':
    all_datasets = generate_datasets(args.data_path, tokenizer, args.class_distrib)
  elif args.mode == 'no_lemma_division':
    all_datasets = generate_mixed_datasets(args.data_path, tokenizer, args.class_distrib)
  output_folder_path = args.output_folder_path

  fold_count = 0
  df_all_lemmas = pd.DataFrame()
  all_results = {}
  for df_train, df_test in all_datasets:
    fold_count += 1
    print('\n\n-------------------------------------------------')
    print('-------------------------------------------------\n\n')
    print('Fold', fold_count, 'out of', len(all_datasets))
    np.random.seed(123)
    torch.manual_seed(123)
    device = torch.device(args.device if torch.cuda.is_available() else "cpu")

    df_train = df_train.sample(frac=1, random_state=2022)
    df_test = df_test.sample(frac=1, random_state=2022)
    target_set = {g:n for n,g in enumerate(list(set(df_train['target'].tolist())))}
    print("Target set", target_set)
    print("Count train targets", Counter(df_train['target'].tolist()))
    print("Count test targets", Counter(df_test['target'].tolist()))

    df_train['target'] = df_train['target'].map(lambda x: target_set[x])
    df_test['target'] = df_test['target'].map(lambda x: target_set[x])

    print("Train/test shape: ", df_train.shape, df_test.shape)

    train_data_loader = create_data_loader(df_train, tokenizer, args.max_len, args.batch_size)
    test_data_loader = create_data_loader(df_test, tokenizer, args.max_len, args.batch_size)

    model = WSIClassifier(len(target_set), args.pretrained_model_name)
    model.encoder.resize_token_embeddings(len(tokenizer))
    model = model.to(device)
    print("Resizing token embedding to", len(tokenizer))

    optimizer = torch.optim.AdamW(model.parameters(), lr=2e-5)
    num_epochs = args.num_epochs
    total_steps = len(train_data_loader) * num_epochs
    scheduler = get_linear_schedule_with_warmup(
      optimizer,
      num_warmup_steps=0,
      num_training_steps=total_steps
    )

    loss_fn = nn.CrossEntropyLoss().to(device)

    best_accuracy = 0
    for epoch in range(num_epochs):
      print(f'Epoch {epoch + 1}/{num_epochs}')
      print('-' * 10)

      train_acc, train_loss = train_epoch(
        model,
        train_data_loader,
        loss_fn,
        optimizer,
        device,
        scheduler,
        len(df_train)
      )


      test_acc, test_loss, preds, targets = eval_model(
        model,
        test_data_loader,
        loss_fn,
        device,
        len(df_test)
      )

      print('EPOCH: ', epoch + 1)
      accuracy = accuracy_score(targets, preds)
      precision = precision_score(targets, preds, average='macro')
      recall = recall_score(targets, preds, average='macro')
      f1_macro = f1_score(targets, preds, average='macro')


      print("Accuracy: ", accuracy)
      print("Precision macro: ", precision)
      print("Recall macro: ", recall)
      print("F1 macro: ", f1_macro)
      if not os.path.exists(output_folder_path):
          os.makedirs(output_folder_path)
      if epoch + 1 == num_epochs:
        output_model_path = os.path.join(output_folder_path, f'fold_{fold_count}_epoch_{epoch + 1}_{args.mode}.bin')
        torch.save(model.state_dict(), output_model_path)
        df_test['preds'] = preds
        df_test['fold'] = fold_count
        df_all_lemmas = pd.concat([df_all_lemmas, df_test], axis=0)
        all_results['fold_' + str(fold_count)] = [accuracy, precision, recall, f1_macro]
    del model
  output_evaluation_path = os.path.join(output_folder_path, f'{args.mode}_evaluation.tsv')
  output_prediction_path = os.path.join(output_folder_path, f'{args.mode}_predictions.tsv')
  df_all_lemmas = df_all_lemmas.sort_values('lemma')
  df_all_lemmas.to_csv(output_prediction_path, encoding='utf8', sep='\t', index=False)
  with open(output_evaluation_path, 'w', encoding='utf8') as f:
    f.write('fold\taccuracy\tprecision\trecall\tf1_macro\n')
    for k,v in all_results.items():
      f.write(k + '\t' + "\t".join([str(x) for x in v]) + '\n')

  print('Done, results written to folder', output_folder_path)

# Word-sense Induction for Buddhist Sanskrit

Code for experiments presented in the paper ['Word-sense Induction on a Corpus of Buddhist Sanskrit Literature'](https://elex.link/elex2023/wp-content/uploads/49.pdf) published in ELEX 2023 proceedings.

## Funding

This work received funding from the NEH (HAA-277246-21).

## Installation, documentation ##

Instructions for installation assume the usage of PyPI package manager.<br/>

Install dependencies if needed: pip install -r requirements.txt <br/>

#### Train a binary classification model :<br/> 

The script train_binary_classifier.py is used for training a transformer model for generating binary word sense predictions for sentence pairs. 
It returns trained models, binary predictions and cross validation evaluation scores for the models :<br/>

```
python -u train_binary_classifier.py --device cuda --class_distrib balanced --data_path data/wsi_lemmas_clean.csv --mode no_lemma_division --pretrained_model_name Matej/bert-base-buddhist-sanskrit-v2 --num_epochs 5 --output_folder_path results
```

**Arguments:**<br/>
**--pretrained_model_name** Transformer model name on Huggingface <br/>
**--data_path** Path to input data containing lemmas with sense labels. <br/>
**--output_folder_path** Path to output folder containing trained models and prediction results. <br/>
**--mode** Two modes are available (see paper for details): 'lemma_division' or 'no_lemma_division'<br/>
**--device** GPU device <br/>
**--class_distrib** Balanced or unbalanced classes in train set <br/>
**--num_epochs** Number of training epochs <br/>
**--max_len** Max input sequence length <br/>
**--batch_size** Batch size <br/>

#### Generate and evaluate word sense clusters :<br/> 

The script custom_clustering.py takes the binary predictions produced by the model in the previous step as input. 
It returns word sense clusters for lemmas and evaluation scores :<br/>

```
python -u custom_clustering.py --input_binary_prediction_file results/no_lemma_division_predictions.tsv --output_folder_path cluster_results --mode no_lemma_division
```

**Arguments:**<br/>
**--input_binary_prediction_file** Path to input file containing binary predictions. <br/>
**--output_folder_path** Path to output folder containing clustering results. <br/>
**--mode** Two modes are available (see paper for details): 'lemma_division' or 'no_lemma_division'<br/>


import pandas as pd
import numpy as np
import os
from collections import defaultdict
from pandas import read_csv
from sklearn.metrics import adjusted_rand_score
from sklearn.metrics import f1_score
import argparse


def gold_predict(df):
    """ This method assigns the gold and predict fields to the data frame. """
    df = df.copy()
    df['gold'] = df['lemma'] + '_' + df['sense']
    df['predict'] = df['lemma'] + '_' + df['cluster_label']
    return df


def ari_per_word_weighted(df):
    """ This method computes the ARI score weighted by the number of sentences per word. """
    df = gold_predict(df)
    words = {word: (adjusted_rand_score(df_word.gold, df_word.predict), len(df_word))
             for word in df.lemma.unique()
             for df_word in (df.loc[df['lemma'] == word],)}

    cumsum = sum(ari * count for ari, count in words.values())
    total  = sum(count for _, count in words.values())

    assert total == len(df), 'please double-check the format of your data'

    return cumsum / total, words


def evaluate_ari(dataset_fpath):
    with open(dataset_fpath.split('.')[0] + '_ari_scores.tsv', 'w', encoding='utf8') as f:
        df = read_csv(dataset_fpath, sep='\t', dtype={'sense': str, 'cluster_label': str})
        ari, words = ari_per_word_weighted(df)
        f.write('Word\tARI\tNum. words\n')

        for word in sorted(words.keys()):
            f.write('{}\t{:.6f}\t{:d}\n'.format(word, *words[word]))

        print('\t{:.6f}\t{:d}'.format(ari, len(df)))
    return ari


def evaluate_f1(dataset_fpath):
    df = pd.read_csv(dataset_fpath, sep='\t',
                     dtype={'sense': str, 'cluster_label': str})
    lemmas = sorted(set(df.lemma.tolist()))
    results = []
    avg_f1 = 0
    for lemma in lemmas:
        weighted_f1 = 0
        df_lemma = df[df['lemma'] == lemma]
        all_gs = set(df_lemma.sense.tolist())
        all_clusters = set(df_lemma.cluster_label.tolist())
        for gs in all_gs:
            max_f1 = 0
            for c in all_clusters:
                df_lemma_copy = df_lemma.copy()
                df_lemma_copy['sense'] = df_lemma_copy['sense'].apply(lambda x: 1 if x == gs else 0)
                df_lemma_copy['cluster_label'] = df_lemma_copy['cluster_label'].apply(lambda x: 1 if x == c else 0)
                true = df_lemma_copy['sense'].tolist()
                pred = df_lemma_copy['cluster_label'].tolist()
                f1 = f1_score(true, pred)
                if f1 > max_f1:
                    max_f1 = f1
            df_lemma_gs = df_lemma[df_lemma['sense'] == gs]
            weight = df_lemma_gs.shape[0] / df_lemma.shape[0]
            weighted_f1 += max_f1 * weight
        avg_f1 += weighted_f1
        results.append((lemma, round(weighted_f1, 3), df_lemma.shape[0]))
    avg_f1_score = round(avg_f1 / len(lemmas), 3)
    results.append(('AVG', avg_f1_score, df.shape[0]))
    df = pd.DataFrame(results, columns=['Word', 'F1', 'Num. words'])
    df.to_csv(dataset_fpath.split('.')[0] + '_f1_scores.tsv', sep='\t', index=False)
    return avg_f1_score


def combine_clusters(clusters, r_count, id2r_count):
    labels = clusters.keys()
    max_clusters = 10

    if len(set(labels)) == 1:
        return clusters

    intersection_treshold = 0.8
    cluster_labels = ()
    cluster_list = list(clusters.items())
    max_dist = 0
    max_labels = ()
    for i in range(len(cluster_list)):
        for j in range(i+1,len(cluster_list)):
            id, c1 = cluster_list[i]
            id2, c2 = cluster_list[j]
            dist = len(c1.intersection(c2))/max(len(c1), len(c2))
            if dist > max_dist:
                max_dist = dist
                max_labels = (id, id2)
    if max_dist > intersection_treshold:
        cluster_labels = max_labels
    if cluster_labels:
        for id in clusters[cluster_labels[0]]:
            clusters[cluster_labels[1]].add(id)
            id2r_count[str(id) + '_' + str(cluster_labels[1])] = r_count
        del clusters[cluster_labels[0]]
        return combine_clusters(clusters, r_count + 1, id2r_count)
    elif len(clusters) > max_clusters:
        max_dist = 0
        cluster_labels = ()
        cluster_list = list(clusters.items())
        for i in range(len(cluster_list)):
            for j in range(i + 1, len(cluster_list)):
                id, c1 = cluster_list[i]
                id2, c2 = cluster_list[j]
                dist = len(c1.intersection(c2)) / max(len(c1), len(c2))
                if dist > max_dist:
                    max_dist = dist
                    cluster_labels = (id, id2)
        if cluster_labels:
            for id in clusters[cluster_labels[0]]:
                clusters[cluster_labels[1]].add(id)
                id2r_count[str(id) + '_' + str(cluster_labels[1])] = r_count
            del clusters[cluster_labels[0]]
            return combine_clusters(clusters, r_count + 1, id2r_count)
    return clusters


def remove_duplicates(clusters):
    cluster_sizes = {c: len(v) for c, v in clusters.items()}
    reverse_dict = defaultdict(set)
    for c in clusters:
        for sent_id in clusters[c]:
            reverse_dict[sent_id].add(c)
    for sent_id, cl_set in reverse_dict.items():
        if len(cl_set) > 1:
            c_sizes = sorted([(x, cluster_sizes[x]) for x in cl_set], key=lambda x: x[1])[:-1]
            for cl, size in c_sizes:
                clusters[cl].remove(sent_id)
            if not clusters[cl]:
                del clusters[cl]
    return clusters


def cluster(df):
    lemmas = sorted(set(df.lemma))
    output = []
    for lemma in lemmas:
        df_lemma = df[df['lemma'] == lemma]
        id2idx_1 = {id: idx for idx, id in enumerate(sorted(set(df_lemma.sent_1_id)))}
        idx2id_1 = {idx: id for idx, id in enumerate(sorted(set(df_lemma.sent_1_id)))}
        id2idx_2 = {id: idx for idx, id in enumerate(sorted(set(df_lemma.sent_2_id)))}
        idx2id_2 = {idx: id for idx, id in enumerate(sorted(set(df_lemma.sent_2_id)))}
        adjacency_matrix = np.zeros((len(set(df_lemma.sent_1_id)), len(set(df_lemma.sent_2_id))))
        id2sense = {}
        for idx, row in df_lemma.iterrows():
            id_1 = row['sent_1_id']
            id_2 = row['sent_2_id']
            if 'sense_1' in df.columns:
                sense_1 = row['sense_1']
                sense_2 = row['sense_2']
                id2sense[id_1] = sense_1
                id2sense[id_2] = sense_2
            pred = row['preds']
            if pred == 0:
                adjacency_matrix[id2idx_1[id_1], id2idx_2[id_2]] = 1
        clusters = {}
        for row_idx, row in enumerate(adjacency_matrix):
            if row_idx not in clusters:
                clusters[row_idx] = set()
                clusters[row_idx].add(idx2id_1[row_idx])
            for col_idx, con in enumerate(row):
                if con:
                    clusters[row_idx].add(idx2id_2[col_idx])
        id2r_count = {}
        ids = clusters.items()
        for c, v in ids:
            for id in v:
                id2r_count[str(id) + '_' + str(c)] = 0
        clusters = combine_clusters(clusters, 0, id2r_count)
        clusters = remove_duplicates(clusters)
        for c in clusters:
            for sent_id in clusters[c]:
                if 'sense_1' in df.columns:
                    output.append((lemma, sent_id, id2sense[sent_id], str(c)))
                else:
                    output.append((lemma, sent_id, str(c)))
    return output


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_binary_prediction_file", default='results/no_lemma_division_predictions.tsv',
                        help="Path to input file containing binary predictions.")
    parser.add_argument("--output_folder_path", default='cluster_results',
                        help="Path to output folder containing clustering results.")
    parser.add_argument("--mode",
                        default='no_lemma_division',
                        help="two modes are available: 'lemma_division' or 'no_lemma_division'")
    args = parser.parse_args()
    if not os.path.exists(args.output_folder_path):
        os.makedirs(args.output_folder_path)

    rf_ari = open(args.output_folder_path + '/ari_results.txt', 'w', encoding='utf8')
    rf_f1 = open(args.output_folder_path + '/f1_results.txt', 'w', encoding='utf8')
    rf_ari.write('file\tfold 1\tfold 2\tfold 3\tfold 4\tfold 5\tAvg.\n')
    rf_f1.write('file\tfold 1\tfold 2\tfold 3\tfold 4\tfold 5\tAvg.\n')

    df = pd.read_csv(args.input_binary_prediction_file, sep='\t')
    folds = sorted(list(set(df.fold.tolist())))
    f_results_ari_all = []
    f_results_f1_all = []

    for fold in folds:
        df_fold = df[df['fold']==fold]
        output = cluster(df_fold)
        df_output = pd.DataFrame(output, columns=['lemma', 'sID', 'sense', 'cluster_label'])
        if args.mode == 'no_lemma_division':
            output_path = os.path.join(args.output_folder_path, 'no_lemma_division_clusters_fold_' + str(fold) + '.tsv')
            onesense_lemmas = set()
            for lemma in sorted(list(set(df_output['lemma'].tolist()))):
                df_lemma = df_output[df_output['lemma'] == lemma]
                senses = df_lemma['sense'].tolist()
                num_senses = len(set(senses))
                if num_senses == 1:
                    onesense_lemmas.add(lemma)
            df_output = df_output[~df_output['lemma'].isin(onesense_lemmas)]
        else:
            output_path = os.path.join(args.output_folder_path, 'lemma_division_clusters_fold_' + str(fold) + '.tsv')
        df_output.to_csv(output_path, sep='\t', index=False)
        ari = evaluate_ari(output_path)
        f1 = evaluate_f1(output_path)
        f_results_ari_all.append(ari)
        f_results_f1_all.append(f1)

    results_all_ari_avg = sum(f_results_ari_all)/len(f_results_ari_all)
    f_results_ari_all.append(results_all_ari_avg)

    results_all_f1_avg = sum(f_results_f1_all) / len(f_results_f1_all)
    f_results_f1_all.append(results_all_f1_avg)


    f_results_ari_all = ['{:.6f}'.format(ari) for ari in f_results_ari_all]
    f_results_f1_all.append(f1)
    f_results_f1_all = ['{:.6f}'.format(f1) for f1 in f_results_f1_all]

    rf_ari.write(args.mode + '_clusters.tsv\t' + "\t".join(f_results_ari_all) + '\n')
    rf_f1.write(args.mode + '_clusters.tsv\t' + "\t".join(f_results_f1_all) + '\n')
    rf_ari.close()
    rf_f1.close()

